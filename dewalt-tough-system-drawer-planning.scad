drawer_width = 450;
drawer_depth = 285;
drawer_height = 100;

box_thickness = 10;
inner_box_offset = box_thickness / 2;

import("Exceed Deck Box.stl");

difference(){
  cube([drawer_width + box_thickness, drawer_depth + box_thickness, drawer_height]);
  translate([inner_box_offset, inner_box_offset, inner_box_offset]){
    cube([drawer_width, drawer_depth, 999]);
  }
}

