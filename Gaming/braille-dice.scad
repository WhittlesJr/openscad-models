include <../lib/PolyDiceGenerator/PolyDiceGenerator.scad>
//include <../lib/3D-Printed-Braille-Labels/Design Files/Braille_Template_Module_MMC.scad>

$fn = 50;

//d(20)

d2=false;
d3=false;
d4=false;
d4c=false;
d4i=false;
d4p=false;
d6=false;
d8=false;
d10=false;
d00=false;
d12=false;
d12r=false;

add_bumpers = true;
bumper_size = 1.0;
text_depth=0.5;

text_font="DejaVu Sans";

d20_size=23;
d20_text_spacing=0.8;
d20_text_size=18;
d20_text_v_push=-2;

d20_text=[
          "\u2801",       // 1
          "\u2811",       // 5
          "\u2801\u2809", // 13
          "\u2801\u2801", // 11
          "\u281B",       // 7
          "\u2801\u2811", // 15
          "\u2801\u280A", // 19
          "\u280B",       // 6
          "\u280A",       // 9
          "\u2801\u2813", // 18
          "\u2819",       // 4
          "\u2801\u2819", // 14
          "\u2801\u281B", // 17
          "\u2801\u280B", // 16
          "\u2809",       // 3
          "\u2801\u2803", // 12
          "\u2803",       // 2
          "\u2803\u281A", // 20
          "\u2813",       // 8
          "\u2801\u281A"  // 10
          ];

d20_underscore_size=30;
d20_underscore_v_push=-11;
d20_underscores=["_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_","_"];
d20_bumpers=[true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true];

//braille_char("A");
