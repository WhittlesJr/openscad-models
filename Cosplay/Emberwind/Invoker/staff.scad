fudge = 1.1;

staff_outer = 39;
staff_thickness = 6.5;
staff_inner = staff_outer - staff_thickness;
staff_height = 1000;
staff_pos_y = -staff_height/1.35;

module staff_area(){
  translate([0,staff_pos_y,0]){
    rotate([90,0,0]){
      cylinder(d=staff_outer,h=staff_height,center=true);
    }
  }
}
module staff(){
  difference(){
    staff_area();
    translate([0,staff_pos_y,0]){
      rotate([90,0,0]){
        cylinder(d=staff_inner,h=staff_height*fudge,center=true);
      }
    }
  }
}
