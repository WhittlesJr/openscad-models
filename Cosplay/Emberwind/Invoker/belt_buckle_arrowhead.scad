include <../../../lib/bend.scad/bend.scad>
include <../../../lib/MCAD/regular_shapes.scad>
include <belt.scad>

$fn = 250;

height = belt_width * 1.45;
width = height * 0.44;
neck_width = width * 0.35;
top_width = width * 0.9;

top_point_y = height *0.85;
neck_y = height * 0.7;
bottom_point_y = height * 0.6;
bulge_height = 8;

module arrowhead_half_bottom(){
  linear_extrude(height=0.01,center=true){
    polygon([[0,0],
             [0,neck_y],
             [neck_width/2,neck_y],
             [width/2,bottom_point_y],
             [0,0]]);
  }
}
module arrowhead_half_top(){
  linear_extrude(height=0.01,center=true){
    polygon([[0,neck_y],
             [0,height],
             [top_width/2,top_point_y],
             [neck_width/2,neck_y],
             [0,neck_y]]);
  }
}

module arrowhead_bulge(){
  translate([0,height/2,0]){
    rotate([90,0,90]){
      linear_extrude(height=0.01,center=true){
        ellipse(height*0.9,bulge_height);
      }
    }
  }
}

module arrowhead_half(cutout_mult){
  hull(){
    difference(){
      union(){
        children();
        mirror([1,0,0]) children();
        arrowhead_bulge();
      }
      translate([0,neck_y-500*cutout_mult,0]){
        cube([1000,1000,1000],center=true);
      }
    }
  }
}

module arrowhead_bottom(){
  arrowhead_half(-1) arrowhead_half_bottom();
}

module arrowhead_top(){
  arrowhead_half(1) arrowhead_half_top();
}

module arrowhead(){
  rotate([09.5+90,0,0]){
    translate([-width/2,-height/2-1,belt_loop_thickness/2+1]){
      cylindric_bend([width,height,bulge_height],-200){
        translate([width/2,0,0]){
          arrowhead_bottom();
          arrowhead_top();
        }
      }
    }
  }
}
belt_loop_width = 5;

module arrowhead_belt_loop(){
  difference(){
    belt_loop(belt_loop_width);
    translate([0,-9,0]){
      cube([999, belt_loop_thickness, 999], center=true);
    }
  }
}


module infill(){
  difference(){
    translate([0,-belt_loop_thickness/2+2,0]){
      scale([2,belt_loop_width,belt_loop_height*0.9]){
        rotate([0,90,0]){
          cylinder(d=1,h=1,center=true);
        }
      }
    }
    belt();
  }
}

arrowhead_belt_loop();
arrowhead();
infill();

//belt();
