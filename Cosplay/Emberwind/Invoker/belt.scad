belt_thickness = 5;
//belt_width = 36.6;
belt_width = 45.2;

belt_loop_wall = 3.5;
belt_loop_thickness = belt_thickness + belt_loop_wall*2;
belt_loop_height = belt_width + belt_loop_wall*2;

module belt(){
  cube(size=[400,belt_thickness,belt_width],center = true);
}

module belt_loop(width){
  difference(){
    cube([width, belt_loop_thickness, belt_loop_height], center=true);
    belt();
  }
}
