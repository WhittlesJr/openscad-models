include <../../../lib/MCAD/regular_shapes.scad>

$fn=150;

ellipse_width = 92;
ellipse_x_radius = ellipse_width/2;
ellipse_height = 56.16*2;

top_width = 32.5;
top_y = 102;
top_corner_x = top_width / 2;

shanpe_depth = 4;

module shanpe_cutout(){
  height = top_y*2;
  width = (ellipse_width - top_corner_x);
  translate([top_corner_x+width/2,height/2,0]){
    ellipse(width,height);
  }
}

module shanpe(){
  difference(){
    union(){
      polygon([[-ellipse_x_radius,0],
               [-top_corner_x,top_y],
               [top_corner_x,top_y],
               [ellipse_x_radius,0]]);
      ellipse(ellipse_width,ellipse_height);
    }
    shanpe_cutout();
    mirror([1,0,0]) shanpe_cutout();
  }
}

module beveled_shanpe(){
  linear_extrude(height=shanpe_depth,scale=0.88){
    shanpe();
  }
}

beveled_shanpe();
