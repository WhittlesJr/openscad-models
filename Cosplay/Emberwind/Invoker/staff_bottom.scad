include <../../../lib/MCAD/regular_shapes.scad>
include <staff.scad>;
include <staff_crescent.scad>;

$fn=150;

staff_bottom_outer = staff_outer *3;
staff_bottom_height = staff_height * 0.3;
staff_bottom_upper_height = staff_bottom_height * 0.7;

staff_bottom_ellipse_width = staff_bottom_outer * 1.2;
staff_bottom_ellipse_height = staff_bottom_ellipse_width * 1.6;

staff_bottom_y = staff_pos_y-staff_height/2 + staff_bottom_height / 9;

function ellipse_offset_for_cross_section(ellipse_width,ellipse_height,cross_section_width) =
  let (x = cross_section_width / 2,
       b = ellipse_width / 2,
       a = ellipse_height / 2)
  sqrt(a^2 * (1 - (x^2 / b^2)));


module staff_bottom_flare(){
  difference(){
    rotate([-90,0,0]){
      cylinder(d1=staff_bottom_outer,d2=staff_outer,h=staff_bottom_upper_height);
    }
    translate([0,staff_bottom_upper_height,0]){
      scale([1,4.1,1]){
        rotate([90,0,0]){
          torus(staff_bottom_outer*1.1,staff_outer/2);
        }
      }
    }
  }
}
module staff_bottom_nub(){
  y_offset = ellipse_offset_for_cross_section(staff_bottom_ellipse_width,staff_bottom_ellipse_height,
                                              staff_bottom_outer);
  difference(){
    translate([0,y_offset,0]){
      scale([staff_bottom_ellipse_width,staff_bottom_ellipse_height,staff_bottom_ellipse_width]){
        sphere(r=0.5);
      };
    }
    translate([0,250,0]){
      cube([500,500,500],center=true);
    }
  }
}

module staff_bottom_ring(){
  rotate([90,0,0]){
    torus(staff_bottom_outer/2+2,staff_bottom_outer/2-2);
  }
}
module staff_bottom(){
  difference(){
    scale([0.85,1,1]){
      difference(){
        translate([0,staff_bottom_y,0]){
          staff_bottom_flare();
          staff_bottom_nub();
          staff_bottom_ring();
        }
        translate([0,-250+staff_bottom_y-40]){
          cube([500,500,500],center=true);
        }
      }
    }
    staff_area();
  }
}

staff_bottom();
//staff();

//rotate([0,0,90]){
//  crescent_whole();
//
//}
