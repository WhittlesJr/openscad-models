include <../../../lib/MCAD/regular_shapes.scad>

module ring_cutout(radius,thickness){
  translate([0, 0, -radius/8]) {
    cylinder(d2=radius*3,d1=radius*2,h=thickness*5);
  }
}

module ring(radius, thickness){
  difference(){
    torus2(radius,thickness);
    ring_cutout(radius,thickness);
    mirror([0,0,1]){
      ring_cutout(radius,thickness);
    }

  }
}
