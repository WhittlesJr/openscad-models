include <bracelet.scad>
include <../../../lib/MCAD/regular_shapes.scad>

$fn = 150;

e = 1.02;

studded_bracelet_thickness = 5.7;
studded_bracelet_height = 20;

stud_height = 18.11;
stud_width = 13.1;
stud_depth = 2.9;
stud_side = 12;
stud_top_edge = 7;
stud_edge = (stud_height - stud_side) / 2;

module stud_quarter(){
  polygon(points = [[0,0],
                    [0,stud_height/2],
                    [stud_top_edge/2,stud_height/2],
                    [stud_width/2,stud_height/2-stud_edge],
                    [stud_width/2,0]]);
}

module stud_half(){
  stud_quarter();
  mirror([1,0,0]) stud_quarter();
}

module stud_cutout(){
  scale([e,e,e]){
    hull(){
      rotate([90,0,0]){
        linear_extrude(height=stud_depth*2){
          union(){
            stud_half();
            mirror([0,1,0]) stud_half();
          }
        }
      }
      scale([stud_width,stud_depth*2,stud_height]){
        sphere(r=0.5);
      }
    }
  }
}

module cutouts(stud_count,tapered_diameter){
  stud_angle = 360 / stud_count;
  for (i = [1 : 15]) {
    a = i * stud_angle ;
    r = tapered_diameter / 2;
    x = cos(a) * r;
    y = sin(a) * r;
    translate([x,y,0]){
      rotate([0,0,a+90]){
        stud_cutout();
      }
    }
  }

}

module studded_bracelet(stud_count, inner_diameter){
  tapered_diameter = (inner_diameter + studded_bracelet_thickness * 2) * 0.95;

  difference(){
    bracelet(inner_diameter, studded_bracelet_thickness, studded_bracelet_height,
             tapered_diameter = tapered_diameter);
    cutouts(stud_count,tapered_diameter);
  }

}
module forearm_bracelet() studded_bracelet(10, 65.5);
module bicep_bracelet()   studded_bracelet(12, 75);

//forearm_bracelet();
bicep_bracelet();


