include <bracelet.scad>
include <ring.scad>

$fn = 150;
jangley_bracelet_inner_diameter = 71.33;
jangley_bracelet_thickness = 5;
jangley_bracelet_height = 14;
jangley_bracelet_outer_radius = outer_radius(jangley_bracelet_inner_diameter, jangley_bracelet_thickness);

module hoop_mount(){
  difference(){
    translate([jangley_bracelet_outer_radius,0,0]){
      ring(jangley_bracelet_inner_diameter/8, jangley_bracelet_thickness/1.5);
    }
    arm_hole(jangley_bracelet_inner_diameter);
  }
}

module jangley_bracelet(){
  bracelet(jangley_bracelet_inner_diameter,jangley_bracelet_thickness,jangley_bracelet_height);
  hoop_mount();
}

jangler_radius = jangley_bracelet_outer_radius/3;
jangler_thickness = jangley_bracelet_thickness/1.2;

module jangler(){
  ring(jangler_radius,jangler_thickness);
}

jangler_1_position = jangley_bracelet_outer_radius+jangler_radius+jangley_bracelet_thickness;
jangler_angler = 50;

module jangler_1(){
  rotate([jangler_angler,0,0]){
    translate([jangler_1_position,0,0]){
      jangler();
    }
  }
}

module jangler_2(){
  rotate([-jangler_angler,0,0]){
    translate([jangler_1_position+jangler_radius,0,0]){
      jangler();
    }
  }
}


jangler_1();
jangler_2();
jangley_bracelet();
