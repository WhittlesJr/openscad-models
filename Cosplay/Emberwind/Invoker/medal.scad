$fn = 100;
e = 0.05;

diameter = 30;
height = diameter / 8.1818;

bezel_width =7.0;
bezel_depth = 1.5;

module inset() {
  cylinder(d = diameter - bezel_width, h = bezel_depth);
}

difference () {
  cylinder(d = diameter, h = height);
  translate([0, 0, height - bezel_depth + e]) {
    inset();
  }
  //translate([0, 0, -e]) {
  //  inset();
  //}
  //translate([0, 0, height - bezel_depth]) {
  //  text("HI", font = "Junicode", size = 12);
  //}
}

ring_diameter = diameter / 4.090;
ring_radius = height / 2;

difference () {
  translate([diameter / 2, 0, height / 2]){
    rotate_extrude(convexity = 10) {
      translate([ring_diameter, 0, 0]) {
        circle(r = ring_radius);
      }
    }
  }
  cylinder(d = diameter, h = height);

}
