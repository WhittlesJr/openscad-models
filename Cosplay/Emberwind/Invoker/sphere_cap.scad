include <../../../lib/ClothBotCreations/utilities/fillet.scad>
include <../../../lib/MCAD/regular_shapes.scad>

$fn = 150;

globe_diameter = 98.5;
globe_radius = globe_diameter / 2;
neck_height = 11;
neck_diameter = 25.6;
neck_radius = neck_diameter / 2;

neck_offset = cos(asin(neck_radius / globe_radius)) * globe_radius;

module neck(){
  translate([0,0,neck_offset]){
    cylinder(h=neck_height,d=neck_diameter);
  }
}

module globe(){
  color("lightblue"){
    sphere(d=globe_diameter);
    neck();
  }
}


module cap_arm(){
  translate([0,-globe_diameter/5,neck_offset*0.8]){
    rotate([-55,0,0]){
      scale([1.3,0.8,2]){
        sphere(d=globe_diameter/4);
      }
    }
  }
}

module top_cap_bulge(){
  outer_diameter = neck_diameter*1.2;
  height = neck_height*1.3;
  bevel_height = height*0.2;
  translate([0,0,neck_offset]){
    hull(){
      translate([0,0,height-bevel_height]){
        torus(outer_diameter/2,outer_diameter/2*0.3);
      }
      cylinder(d=outer_diameter,h=height-bevel_height);
    }
  }
}
module cap_arms(){
  cap_arm();
  mirror([0,1,0]) cap_arm();
  mirror([1,1,0]) cap_arm();
  mirror([-1,1,0]) cap_arm();

}

module center_hole(){
  cylinder(d=neck_diameter/3.5,h=999,center=true);
}

module top_cap_mound(){
  translate([0,0,neck_offset*0.8]){
    cylinder(d1=neck_diameter*2,d2=neck_diameter*0.8,h=neck_height*2);
  }
}

module top_cap(){
  color("khaki"){
    difference(){
      //fillet(r=1,steps=5){
      union(){
        top_cap_bulge();
        top_cap_mound();
        cap_arms();
      }
      globe();
      center_hole();
    }
  }
}

module bottom_cap(){
  color("khaki"){
    difference(){
      rotate([180,0,0]){
        //union(){
        fillet(r=1,steps=5){
          cap_arm();
          mirror([0,1,0]) cap_arm();
          mirror([1,1,0]) cap_arm();
          mirror([-1,1,0]) cap_arm();
        }
      }
      globe();
      center_hole();
    }
  }
}

//globe();
//top_cap();

bottom_cap();
