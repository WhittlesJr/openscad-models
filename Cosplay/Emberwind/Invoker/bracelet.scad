e = 10.0;

function outer_radius(inner_diameter, thickness) =
  let (outer_diameter = (thickness * 2) + inner_diameter)
  outer_diameter/2;

module arm_hole(inner_diameter){
  translate([0,0,-e]) {
    cylinder(d = inner_diameter, h = 99);
  }
}

module bracelet_half(inner_diameter, thickness, height, tapered_diameter){

  outer_diameter = outer_radius(inner_diameter, thickness) * 2;

  tapered_diameter = (tapered_diameter == undef) ?
    outer_diameter*0.95 : tapered_diameter;

  difference(){
    cylinder(d1=outer_diameter, d2=tapered_diameter, h=height/2);
    arm_hole(inner_diameter);
  }

}

module bracelet(inner_diameter, thickness, height, tapered_diameter = undef){
  bracelet_half(inner_diameter, thickness, height, tapered_diameter);
  mirror([0,0,-1]) bracelet_half(inner_diameter, thickness, height, tapered_diameter);
}
