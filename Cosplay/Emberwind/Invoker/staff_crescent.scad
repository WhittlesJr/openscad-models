include <ring.scad>
include <staff.scad>

$fn = 200;

crescent_diameter = 306;
crescent_width = 55;
crescent_cutout_offset_angle = 45;
crescent_cutout_offset_dist = crescent_diameter * 0.11;
horn_height = crescent_diameter * 0.12;
horn_left_angle = 195;
horn_top_angle = 85;
stem_length = crescent_diameter * 0.7;
stem_position = -crescent_diameter/1.9-stem_length / 1.3;
stem_diameter = 25.9;

magnet_diameter = 12.78 * fudge;
magnet_height = 2.15 * fudge;
magnet_depth = magnet_height*2.3;

module magnet_indent(side){
  translate([stem_position*0.9,stem_diameter/2*side-(magnet_depth/2*side),0]){
    rotate([90,0,0]){
      cylinder(d=magnet_diameter, h=magnet_depth, center=true);
    }
  }
}
module magnet(side){
  translate([stem_position*0.9,stem_diameter/2*side-(magnet_depth/2*side),0]){
    rotate([90,0,0]){
      cylinder(d=magnet_diameter, h=magnet_height, center=true);
    }
  }
}
module stem(){
  difference(){
    translate([stem_position,0,0]) {
      rotate([0,90,0]){
        cylinder(d=stem_diameter, h=stem_length);
      }
    }
    magnet_indent(-1);
    magnet_indent(1);
  }
  //magnet(-1);
  translate([-crescent_diameter/1.75,0,0]) {
    scale([1,1.2,0.45]) {
      rotate([0,90,0]){
        cylinder(d1=stem_diameter*0.8,d2=stem_diameter*3,h=crescent_diameter*0.2);
      }
    }
  }
}


module horn(offset_multiplier, angle){
  offset=offset_multiplier * crescent_diameter / 2;
  scale([1,1,0.25]) {
    translate([offset * sin(angle), -offset * cos(angle),0]) {
      rotate([90,0,angle]) {
        cylinder(d1=crescent_width/1.4,d2=0,h=horn_height);
      }
    }
  }
}

module outer_disk_half() {
  cylinder(d1=crescent_diameter, d2=crescent_diameter*0.6, h=crescent_width/2, $fn=250);
}

module outer_disk(){
  outer_disk_half();
  mirror([0,0,1]){
    outer_disk_half();
  }
}

module conical_cutout_half(){
  cylinder(d2=crescent_diameter*1.4, d1=0, h=crescent_width * 4, $fn=250);
}

module conical_cutout(){
  translate([crescent_cutout_offset_dist * sin(crescent_cutout_offset_angle),crescent_cutout_offset_dist * cos(crescent_cutout_offset_angle),-crescent_width*2.27]){
    conical_cutout_half();
  }
}

module notch(angle, depth){
  translate([depth * cos(angle), depth * sin(angle),0]) {
    rotate([0,0,angle]) {
      scale([2,1,1]) {
        rotate([0,0,45]) {
          cube(size=[crescent_width/2,crescent_width/2,crescent_width/2], center=true);
        }
      }
    }
  }
}

ring_offset = (crescent_diameter/2)*0.95;
ring_pin_diameter = crescent_diameter * 0.015;
ring_pin_height = crescent_width*0.3;
ring_radius = crescent_width / 1.5;
ring_thickness = crescent_width / 6;

ring_angle_1=143;
ring_angle_2=160;
ring_angle_3=210;
ring_angle_4=230;

module hole(angle){
  translate([ring_offset*cos(angle),ring_offset*sin(angle),0]) {
    cylinder(d=ring_pin_diameter * 1.2,h=crescent_width*2, center=true);
  }
}

module ring_pin(angle){
  cylinder(d=ring_pin_diameter,h=ring_pin_height, center=true);
}


module crescent_ring(){
  difference(){
    ring(ring_radius,ring_thickness);
    translate([0,ring_radius*1.4,0]) {
      rotate([-90,0,0]) {
        rotate([0,0,45]){
          cylinder(d2=ring_pin_height*3.5, d1=0, h=ring_radius*3, center=true, $fn=4);
        }
      }
    }
  }
}

module crescent_ring_with_pin(angle){
  translate([ring_offset*cos(angle),ring_offset*sin(angle),0]) {
    difference(){
      translate([-ring_radius*1.13,0,0]){
        rotate([90,90,0]){
          crescent_ring();
        }
      }
      scale([1.1,1.1,1.3]){
        ring_pin(angle);
      }
    }
  }
}

module crescent_rings(){
  crescent_ring_with_pin(ring_angle_1);
  crescent_ring_with_pin(ring_angle_2);
  crescent_ring_with_pin(ring_angle_3);
  crescent_ring_with_pin(ring_angle_4);
}

module crescent_whole(){
  difference(){
    outer_disk();
    conical_cutout();
    mirror([0,0,1]){
      conical_cutout();
    }
    notch(325,crescent_diameter*0.3);
    notch(225,crescent_diameter*0.22);
    hole(ring_angle_1);
    hole(ring_angle_2);
    hole(ring_angle_3);
    hole(ring_angle_4);
    hole(115);
    hole(165);
    hole(195);
    hole(245);
  }

  stem();
  horn(0.925, horn_left_angle);
  scale([1,1,0.55]) {
    horn(0.96, horn_top_angle);
  }

}

build_xy = 220;
build_z = 300;

module build_area() {
  cube(size=[build_xy,build_xy,build_z], center=true);
}

module mortise_tenon_left(){
  translate([-30, -115,0]) {
    rotate([0,0,45]) {
      cube(size=[crescent_width*0.2, crescent_width*0.3, crescent_width*0.1], center=true);
    }
  }
}

module mortise_tenon(offset_multiplier,angle,theta){
  offset=offset_multiplier*crescent_diameter/2;
  translate([offset*cos(angle), offset*sin(angle),0]) {
    rotate([0,0,angle+theta]) {
      cube(size=[crescent_width*0.05, crescent_width*0.3, crescent_width*0.1], center=true);
    }
  }
}

module mortise_tenon_left(){
  mortise_tenon(0.90,112,20);
}
module mortise_tenon_right(){
  mortise_tenon(0.78,228,0);
}
module mortise_tenon_top(){
  mortise_tenon(0.9,323,0);
}
module mortise_tenon_stem(){
  scale([3,1,1]){
    mortise_tenon(0.5, 180, 90);
  }
}

build_offset_x=-120;
build_offset_y=20;

module center_build_area(){
  translate([build_offset_x,build_offset_y,0]){
    build_area();
  }
}

module center(){
  difference(){
    intersection(){
      crescent_whole();
      center_build_area();
    }
    mortise_tenon_left();
    mortise_tenon_right();
    mortise_tenon_stem();
  }
}

module left_build_area(){
  translate([0,build_offset_y+build_xy,0]){
    build_area();
  }
}

module left(){
  difference(){
    intersection(){
      crescent_whole();
      left_build_area();
    }
  }
  mortise_tenon_left();
}

module stem_build_area(){
  translate([-build_xy+build_offset_x,0,0]){
    build_area();
  }
}

module top_build_area(){
  translate([build_xy,0,0]){
    build_area();
  }
}

module right(){
  difference(){
    crescent_whole();
    center_build_area();
    left_build_area();
    translate([-20,0,0]) stem_build_area();
    top_build_area();
    mortise_tenon_top();
    mortise_tenon_right();
  }
}

module top(){
  difference(){
    intersection(){
      crescent_whole();
      top_build_area();
    }
    mortise_tenon_top();
  }
}

module stem_extra(){
  difference(){
    intersection(){
      crescent_whole();
      stem_build_area();
    }
    mortise_tenon_stem();
  }
}

rotate([0,0,90]) {
  //center();
  //left();
  //right();
  //top();
  //crescent_whole();
  //crescent_rings();

  //stem_extra();

}
//difference(){
//  crescent_ring_with_pin(0);
//  scale([1.1,1.1,1.1]){
//    ring_pin(0);
//  }
//}

//rotate([0,90,0]) scale([0.90,0.90,0.90]) mortise_tenon(0,0,0);
//!scale([0.9,0.9,0.9]){
//  mortise_tenon_stem();
//}

ring_pin(0);
