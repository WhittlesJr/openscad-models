include <../../../lib/MCAD/regular_shapes.scad>

$fn = 150;

chonky_bracelet_inner_diameter = 70;
chonky_bracelet_outer_diameter = chonky_bracelet_inner_diameter * 1.2;

module rim(){
  torus(chonky_bracelet_inner_diameter/1.95,chonky_bracelet_inner_diameter/2.05);
}
module chonky_bracelet(){
  height = 10.5 * 2;
  difference(){
    union(){
      torus(chonky_bracelet_outer_diameter/2.1,chonky_bracelet_inner_diameter/5);
      translate([0,0,height / 2]){
        rim();
      }
      translate([0,0,height / -2]){
        rim();
      }
    }
    cylinder(d=chonky_bracelet_inner_diameter,h=999,center=true);
  }
}

chonky_bracelet();
