include <staff_crescent.scad>;
include <staff.scad>;

cap_thickness = 5;
cap_height = 15;
cap_position = stem_position+stem_length * 0.4;
cap_outer = staff_outer+cap_thickness;

module cap(){
  difference(){
    translate([0,cap_position,0]){
      rotate([270,0,0]){
        union(){
          translate([0,0,cap_height/2]){
            cylinder(d1=cap_outer,d2=0,h=cap_height*1.5);
          }
          difference(){
            cylinder(d=cap_outer,h=cap_height,center=true);
            cylinder(d=staff_outer,h=cap_height*fudge,center=true);
          }
        }
      }
    }
    rotate([0,0,90]) scale([1.01,1.01,1.01])stem();
  }

}

staff();
!cap();
