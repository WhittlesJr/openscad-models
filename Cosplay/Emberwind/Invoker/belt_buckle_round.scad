include <../../../lib/ClothBotCreations/utilities/fillet.scad>
include <../../../lib/MCAD/regular_shapes.scad>
include <belt.scad>

$fn = 150;

round_buckle_outer_radius = belt_width/2+6;
round_buckle_inner_radius = belt_width/2.3;

buckle_extension_length = 23;
buckle_extension_height = 7;
buckle_extension_center_x = buckle_extension_length / 2 + round_buckle_outer_radius+1;
buckle_extension_center_y = belt_thickness/2+belt_loop_wall/2;

module buckle_extension_cutout(){
  length = 10;
  width = 3;
  translate([-width*2,0,length+3.2]){
    rotate([90,75,0]){
      oval_prism(belt_loop_wall+1,width,length,center=true);
    }
  }
}

module buckle_extension_cutouts(){
  translate([buckle_extension_center_x,buckle_extension_center_y,0]){
    buckle_extension_cutout();
    mirror([1,0,0]) buckle_extension_cutout();
    mirror([0,0,1]) buckle_extension_cutout();
    mirror([1,0,0]) mirror([0,0,1]) buckle_extension_cutout();
  }
}

module buckle_extension(){
  thickness = belt_loop_wall;
  difference(){
    union(){
      translate([buckle_extension_center_x-3,buckle_extension_center_y,0]){
        cube([buckle_extension_length,thickness,buckle_extension_height],center=true);
      }
      difference(){
        translate([buckle_extension_center_x,0,0]){
          belt_loop(belt_loop_width);
        }
        buckle_extension_cutouts();
      }
      translate([buckle_extension_center_x+buckle_extension_length/2-buckle_extension_height/2,buckle_extension_center_y,0]){
        rotate([90,0,0]){
          cylinder(d=buckle_extension_height,h=thickness,center=true);
        }
      }
    }
  }
}

module round_buckle(bulge = true){
  translate([0,belt_thickness/2,0]){
    difference(){
      //fillet(r=1,steps=5){
      union(){
        rotate([90,0,0]){
          hull(){
            torus(round_buckle_outer_radius, round_buckle_inner_radius);
          }
        }
        if (bulge) {
          scale([1,0.7,1]){
            sphere(r=round_buckle_outer_radius*0.88);
          }
        }
      }
      if (!bulge) {
        translate([0,belt_thickness*1.15,0]){
          scale([round_buckle_outer_radius,4,round_buckle_outer_radius]){
            sphere(r=1);
          }
        }
      }
      translate([-150,0,150]){
        rotate([180,0,0]){
          cube([300,300,300]);
        }
      }
    }
  }
}

//belt();
belt_loop_width = 7;
belt_loop_offset = 8;

module loop_cutoff(){
  translate([-100,belt_thickness/2,-100]){
    cube([200,200,200]);
  }

}
module main_loop(){
  difference() {
    belt_loop(belt_loop_width*3);
    loop_cutoff();
  }
}

module buckle_design(bulge){
  fillet(r=1,steps=5){
    //union(){
    round_buckle(bulge);
    buckle_extension();
  }

}
module convex_buckle(){
  buckle_design(true);
  main_loop();
}

module concave_buckle(){
  difference(){
    union(){
      buckle_design(false);
      main_loop();

    }
    rotate([180,0,0]){
      loop_cutoff();
    }
  }
  difference(){
    mirror([0,1,0]){
      buckle_design(false);
    }
    buckle_extension_cutouts();
  }
}


//belt();

//convex_buckle();
concave_buckle();
