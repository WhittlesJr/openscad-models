include <../../../lib/MCAD/regular_shapes.scad>

$fn = 150;


module earring_main_hoop(diameter, thickness){
  torus2(diameter/2,thickness);
}

feather_tube_thickness = 2;
feather_tube_angle_offset = 45;

feather_width = 3.7;
feather_length = 6.35;
feather_tube_wall = 1;

module feather_tube(diameter,thickness,angle){
  offset = diameter/2 + thickness/2 + (feather_length / 2);
  radius = (feather_width / 2) + feather_tube_wall;
  translate([cos(angle)*offset,sin(angle)*offset,0]){
    rotate([90,0,angle+90]){
      cylinder_tube(feather_length,radius,feather_tube_wall,center=true);
    }
  }
}

jump_ring_width = 1;
jump_ring_diameter = 6.2;
jump_ring_radius = jump_ring_diameter/2;

module jump_ring_hole(){
  cube(size=[jump_ring_radius,jump_ring_radius,jump_ring_width],center=true);
}

module earring_hoop(diameter,thickness){
  thickness = diameter / 10.1;
  difference(){
    union(){
      earring_main_hoop(diameter,thickness);
      feather_tube(diameter,thickness,-90);
      feather_tube(diameter,thickness,-90-feather_tube_angle_offset);
    }
    translate([0,diameter/2+thickness-jump_ring_radius/2]){
      jump_ring_hole();
    }
  }

}

module earring_top_hoop(){
  diameter = 50;
  thickness = diameter/ 10.1;
  difference(){
    union(){
      earring_hoop(diameter,thickness);
      feather_tube(diameter,thickness,-90+feather_tube_angle_offset);
    }
    translate([0,diameter/2-thickness+jump_ring_radius/2-0.06]){
      jump_ring_hole();
    }
  }

}

module earring_bottom_hoop(){
  diameter = 40;
  thickness = diameter/ 10.1;
  translate([0,-100,0]){
    earring_hoop(diameter,thickness);
  }
}
earring_top_hoop();
earring_bottom_hoop();
