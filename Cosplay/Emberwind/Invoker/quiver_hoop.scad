include <../../../lib/MCAD/regular_shapes.scad>

$fn=150;

quiver_d1 = 117.6;
quiver_d2 = 138;
quiver_wall = 6.5;
quiver_length = 527.05;

module quiver_hollow(){
  rotate([0,90,0]){
    cylinder(d1=quiver_d1-quiver_wall,d2=quiver_d2-quiver_wall,h=quiver_length+5,center=true);
  }

}


function quiver_y(x) =
  let (x1 = quiver_length / -2,
       x2 = quiver_length / 2,
       y1 = quiver_d1 / 2,
       y2 = quiver_d2 / 2,
       slope = ((y2-y1) / (x2-x1)),
       y_intercept = y1 - (slope * x1))
  (slope * x) + y_intercept;


quiver_hole_diameter = 15;
quiver_hole_1_from_right = 84;
quiver_hole_2_from_right = 158.5;
quiver_hole_3_from_right = 365.125;
quiver_hole_4_from_right = 441.325;

quiver_hole_1_spacing = quiver_hole_2_from_right - quiver_hole_1_from_right;
quiver_hole_2_spacing = quiver_hole_4_from_right - quiver_hole_3_from_right;

quiver_hole_1_x = (quiver_length / 2 - quiver_hole_1_from_right);
quiver_hole_2_x = (quiver_length / 2 - quiver_hole_2_from_right);
quiver_hole_3_x = (quiver_length / 2 - quiver_hole_3_from_right);
quiver_hole_4_x = (quiver_length / 2 - quiver_hole_4_from_right);

quiver_hoop_1_x = quiver_hole_1_spacing / 2 + quiver_hole_2_x;
quiver_hoop_2_x = quiver_hole_2_spacing / 2 + quiver_hole_4_x;

module rivet(x,angle_mult){
  d=12;
  h=20;
  stop_d = d * 5;
  stop_h = 9;
  stop_y = quiver_y(x)-5;
  difference(){
    union(){
      translate([x-(2*angle_mult),quiver_y(x),0]){
        rotate([90,0,40*angle_mult]){
          cylinder(d=7,h=h,center=true);
        }

      }
      intersection(){
        translate([x,stop_y,0]){
          rotate([90,0,0]){
            cylinder(d=stop_d,h=stop_h,center=true);
          }
        }
        quiver_hollow();
      }
    }
    translate([x,stop_y-10-stop_h/2,0]){
      cube([stop_d,20,stop_d],center=true);
    }
  }
}

module quiver_hoop(x){
  outer_diameter = 126;
  outer_radius = outer_diameter/2;

  inner_diameter = 98;
  inner_radius = inner_diameter/2;

  difference(){
    scale([1,1,1]){
      difference(){
        translate([x,quiver_y(x)+38,0]){
          torus(outer_radius, inner_radius);
        }
        quiver_hollow();
      }
    }
  }
}


module quiver_hole(from_right){
  height=200;
  translate([quiver_length/2-from_right,height/2,0]){
    rotate([-90,0,0]){
      cylinder(d=quiver_hole_diameter,h=height,center=true);
    }
  }
}

module quiver(){
  difference(){
    rotate([0,90,0]){
      cylinder(d1=quiver_d1,d2=quiver_d2,h=quiver_length,center=true);
    }
    quiver_hollow();
    quiver_hole(quiver_hole_1_from_right);
    quiver_hole(quiver_hole_2_from_right);
    quiver_hole(quiver_hole_3_from_right);
    quiver_hole(quiver_hole_4_from_right);
  }
}


module quiver_hoop_1_rivet_1() rivet(quiver_hole_1_x,-1);
module quiver_hoop_1_rivet_2() rivet(quiver_hole_2_x,1);
module quiver_hoop_2_rivet_1() rivet(quiver_hole_3_x,-1);
module quiver_hoop_2_rivet_2() rivet(quiver_hole_4_x,1);

module quiver_hoop_1(){
  difference(){
    quiver_hoop(quiver_hoop_1_x);
    quiver_hoop_1_rivet_1();
    quiver_hoop_1_rivet_2();
  }
}
module quiver_hoop_2(){
  difference(){
    quiver_hoop(quiver_hoop_2_x);
    quiver_hoop_2_rivet_1();
    quiver_hoop_2_rivet_2();
  }
}

quiver();

quiver_hoop_1();
quiver_hoop_2();

//quiver_hoop_1_rivets();
//quiver_hoop_2_rivets();

// Testing quiver_y
//translate([-130,quiver_y(-130),0]) sphere(r=10);
//translate([250,quiver_y(250),0]) sphere(r=10);
