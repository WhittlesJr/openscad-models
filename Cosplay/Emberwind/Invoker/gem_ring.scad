include <../../../lib/MCAD/regular_shapes.scad>

$fn = 150;

index_finger_diameter = 15.8;
right_ring_finger_diameter = 15.7;
pinky_finger_diameter = 12.6;


diameter = right_ring_finger_diameter;
radius = diameter / 2;
thickness = 2;

setting_depth = 4.3;
setting_width = 10.8;

module finger(){
  cylinder(d=diameter,h=999,center=true);
}
module place_setting(){
  y = radius+setting_depth;
  translate([0,y,0]){
    rotate([90,0,0]){
      children();
    }
  }
}
module setting_hole(){
  place_setting() {
    cylinder(d1=setting_width,d2=0,h=setting_depth);
  }

}
module setting(){
  scalar = 1;
  place_setting() cylinder(d=setting_width*1.1,h=2);
}

difference(){
  hull(){
    torus(radius+thickness,radius-thickness);
    setting();
  }
  translate([0,0.01,0]){
    setting_hole();
  }
  finger();
}
