$fn = 150;

pipe_diameter = 38.5;
widest_diameter = pipe_diameter * 1.5;
height = 60;

module half(){
  difference() {
    cylinder(d1=widest_diameter,d2=pipe_diameter, h=height/2);
    cylinder(d=pipe_diameter,h=height*2,center=true);
  }
}

half();
mirror ([0,0,1]){
  half();
}
