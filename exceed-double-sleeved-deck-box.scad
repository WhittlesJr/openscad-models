//#############################################
// Parametized card case SCAD file
// written by Norman Deschamps
// Version 4 - October 2019
// Version 3 - Fixed honeycombing so that it properly sits in the middle of the front or back
// Version 4 - Added the option for slots on the side of the box so it can be used to hold the cards
//             while gaming
//#############################################
// Parameterized card case provides a number of options
// Obviously, you can change the internal dimensions to fit any cards.
// You can also add in spacers in the middle of the box to create separate spaces in the box
// The front and back of the box can have honeycombed cutouts, engraved/embossed images, or text
// The front and back can have different embellishments
// Other things that can be changed include: lid height, fillet size (i.e. roundedness of corners, wall thickness, spacer thickness

// Thanks to:
// Thingiverse user Lufton https://www.thingiverse.com/thing:2404395 for the initial customizable design I used as a 
// reference, plus some of their code for adding the images and text
//#############################################

$fn= 100; //controls level of smoothness. 100 is a good comprimise betweeen compile speed and corner smoothness
//=======================================================================
//=========== Dimensions You Need to Change For Your Box ======================
//=======================================================================
//Inside Box dimensions
deck_thicknesses = [28, 4.5]; //y coma separated deck thicknesses; dictates internal box width
l = 67.5; //x
h = 94; //z

// Choose what part you want to build
part = "both"; // [box:Box, top:Top, both:Both]

// Choose your front and back surface embellishments and side slots
front_surface = "none"; // [honeycomb, image, text, none]
back_surface = "none"; // [honeycomb, image, text, none]
side_slots = "no"; //[yes, no]

//++++++++++++ OTHER BOX PARAMETERS ++++++++++++++++++++++++++++++++++++++
//Bits you probably don't need to change
grab_cutout_radius=14;
top_depth = 13; //15 The thinner portion at the top of the main box. Always has to be at least as much as the fillet value
wall_bottom = 2.4; 
wall_top = 1; //0.8//should be at least two perimeters + gap, so 1.0 for 0.4 mm nozzle
fillet = 2; //Roundness of the edges. fillet MUST BE <= wall_bottom + wall_top
deck_spacer = .8; //width of interior spacer panels. Should be at least 0.8 for 0.4mm nozzle for strength
gap = .4; // [.0:0.02:1.0] how tight the fit between box and lid
min_sides = 20;

//++++++++++++ TEXT PARAMETERS ++++++++++++++++++++++++++++++++++++++
text_1 = "Cards!"; //
text_2 = "Awesome!";
text_size = 12; //[.1:.1:50]
text_font = "Arial"; //You can choose any font that you have installed on your computer. "Arial" or "Calibri" are good defaults
text_extrusion = 0.6;
top_text_z_offset = 10;
top_text_x_offset = 0;
bottom_text_z_offset = 10;
bottom_text_x_offset = -5;

//++++++++++++ LOGO PARAMETERS ++++++++++++++++++++++++++++++++++++++++++
//IMAGE VARIABLES ARE AT THE BOTTOM OF THE FILE
//Front Logo Parameters
f_engrave_depth = .8; // [-10:.1:10] // Depth of text and logo engrave. Depth < 0 will make relief.
f_logo_scale = 90; // [0:0.1:100] // Logo scale in percentage of box size

//Back Logo Parameters
b_engrave_depth = -.8; // [-10:.1:10] // Depth of text and logo engrave. Depth < 0 will make relief.
b_logo_scale = 90; // [0:0.1:100] // Logo scale in percentage of box size

//++++++++++++ HONEYCOMB PARAMETERS ++++++++++++++++++++++++++++++++++++++++++
hc_cell_size = 5;
hc_wall_thickness = 1.6; //starts to get fragile at 1. recommend 1.5 minimum
hc_border = .1;

//============================================================
//========= DO NOT CHANGE ANYTHING BELOW THIS LINE!!!!=======
//============================================================

//Computed effective width of the inside of the box taking into account all internal areas and spacers 
width = sum(deck_thicknesses) + (len(deck_thicknesses) - 1) * deck_spacer;

//Computed outside lower bottom box dimensions :: olb
o_x = l + (2*wall_bottom - 2*fillet);
o_y = width + (2*wall_bottom - 2*fillet);
o_h = h + (wall_bottom - 2*fillet) - top_depth;

//Computed outside upper bottom box dimensions :: oub
oub_x = l + 2*wall_top;
oub_y = width + 2*wall_top;
oub_z = top_depth;

//Computed outside upper bottom box translations :: oub_t
//oub_t_xy = wall_bottom - wall_top;
oub_t_z = ((h + wall_bottom) / 2); //
//oub_t_z = (o_h+top_depth)/2;
//oub_t_z = h/2;

//Computed inside translations i_t
i_t_xy = -(width / 2);
i_t_z = (top_depth + wall_bottom) / 2;
//i_t_z = ((h - o_h) + (wall_bottom)) / 2;

//Computed inside top dimensions :: it
it_x = l + 2*(wall_top + gap);
it_y = width + 2*(wall_top + gap);

//Computed text translations tt_t (top text) bt_t (bottom text)
//text_gaps = ((panelHeight - fillet)/sin(60) - 2*text_size)/3;
tt_t_z = o_h/2 - top_text_z_offset - text_size;
tt_t_x = top_text_x_offset;
tbt_t_fy = (o_y + (2*fillet) - f_engrave_depth) / 2 - 0.1;
tbt_t_by = -(o_y + (2*fillet) - f_engrave_depth) / 2 + 0.1;
bt_t_z = -o_h/2 + bottom_text_z_offset;
bt_t_x = bottom_text_x_offset;

//Calculated honeycomb size parameters
hc_y = (o_y + 10)/2; //Just make it wide enough to cover the whole half box
hc_x = l - 2*wall_bottom;
hc_z = o_h - 2*wall_bottom;

//honeycomb translations
hct_y = -o_y/2 + fillet;
hct_x = -(l + (2*wall_bottom)) /2 + 2*wall_bottom;
hct_z = -o_h/2 + wall_bottom;

//side slot size and translations
s_x = wall_bottom;
s_y = width - 2*min_sides;
s_z = h + 2*wall_bottom;
s_t_x = o_x/2;
s_t_y = -o_y/2 + min_sides;
s_t_z = -o_h/2 - wall_bottom;

//============Main Program======================================
if (part == "box") {
    box();
}
else if (part == "top") {
    translate([0, 0, fillet + top_depth/2]) top();
}
else if (part == "both"){
    translate([0, (width/2) + fillet + 10, fillet + o_h/2]) box();
    translate([0, -(width/2) - fillet - 10, fillet + top_depth/2]) top();
}
else {
    translate([0, (width/2) + fillet + 10, fillet + o_h/2]) box();
    translate([0, -(width/2) - fillet - 10, fillet + top_depth/2]) top();
}

//============ FUNCTIONS =================================================

//--------------------------------------------------------------------------------------------------------------------
//Function used for calculating the spacer locations 
function sum(v, i=0, j=100500) = len(v) > i && i <= j ? v[i] + sum(v, i+1, j) : 0;
//Function used for the logo module
function isArray(v) = len(v) != undef;
//--------------------------------------------------------------------------------------------------------------------
module box(){
    //put grab cutout semicircle across the top of the box
    difference(){
    //add spacers if any
        union(){
            //create outside box
            difference(){
                union(){
                    //create the thick lower part of the bottom box
                    minkowski(){
                        cube([o_x, o_y, o_h], true);
                        sphere(fillet); 
                    }
                   //create thin upper part of the bottom box 
                   translate([0, 0, oub_t_z])cube([oub_x, oub_y, oub_z], true);
                    //The following if statements add the box embellishments such as images, text, and hoenycombs.
                    //They go before or after the union depending on whether elements are added (embossments) or
                    //removed (honeycombs, engravings)
                   //add an embossed logo, if selected
                    if (front_surface == "image" && f_engrave_depth > 0)
                    {
                        //add extra 0.1 to y translate just so image is stuck into the box a bit
                         rotate([0,0,180])
                            translate([0, -(o_y + (2*fillet) + f_engrave_depth) / 2 + 0.1,0])
                                logo(front_image, f_engrave_depth, f_logo_scale); 
                    }                
                    if (back_surface == "image" && b_engrave_depth > 0)
                    {
                        //add extra 0.1 to y translate just so image is stuck into the box a bit
                        translate([0, -(o_y + (2*fillet) + b_engrave_depth) / 2 + 0.1,0])
                            logo(back_image, b_engrave_depth, b_logo_scale); 
                    }
                    //add text, if selected
                    if (front_surface == "text"){
                        if (text_1 != ""){
                            translate([tt_t_x, tbt_t_fy, tt_t_z])
                                rotate([90, 0, 180])
                                    linear_extrude(height = wall_bottom + text_extrusion)
                                        text(text_1, halign = "center", valign = "baseline", size = text_size, font = text_font);
                            }
                        if (text_2 != ""){
                            translate([bt_t_x, tbt_t_fy, bt_t_z])
                                rotate([90, 0, 180])
                                    linear_extrude(height = wall_bottom + text_extrusion)
                                        text(text_2, halign = "center", valign = "baseline", size = text_size, font = text_font);
                        }
                    } 
                    if (back_surface == "text"){
                        if (text_1 != ""){
                            translate([tt_t_x, tbt_t_by, tt_t_z])
                                rotate([-90, 180, 180])
                                    linear_extrude(height = wall_bottom + text_extrusion)
                                        text(text_1, halign = "center", valign = "baseline", size = text_size, font = text_font);
                            }
                        if (text_2 != ""){
                            translate([bt_t_x, tbt_t_by, bt_t_z])
                                rotate([-90, 180, 180])
                                    linear_extrude(height = wall_bottom + text_extrusion)
                                        text(text_2, halign = "center", valign = "baseline", size = text_size, font = text_font);
                        }
                    }                   
                  
                  
                  
                    
                } 
                //Add honeycombs if selected
                 if (front_surface == "honeycomb"){
                    translate([hct_x, -2*hct_y, hct_z])
                        rotate(a=[90,0,0])
                            honeycomb(hc_x, hc_z, hc_y, hc_cell_size, hc_wall_thickness, hc_border); 
                }
                if (back_surface == "honeycomb"){
                    translate([hct_x, hct_y, hct_z])
                        rotate(a=[90,0,0])
                            honeycomb(hc_x, hc_z, hc_y, hc_cell_size, hc_wall_thickness, hc_border); 
                }
                //Add engraved images if selected
                if (front_surface == "image" && f_engrave_depth < 0)
                    {
                        rotate([0,0,180])
                            translate([0, -(o_y + (2*fillet) + f_engrave_depth) / 2,0])
                                logo(front_image, -f_engrave_depth, f_logo_scale); 
                    }            
                if (back_surface == "image" && b_engrave_depth < 0)
                    {
                        translate([0, -(o_y + (2*fillet) + b_engrave_depth) / 2,0])
                            logo(back_image, -b_engrave_depth, b_logo_scale); 
                    }
                    
                //create inside hole of box
                translate([0, 0, i_t_z])cube([l, width, h], true);     
            } 
            //Add in spacers if we have more than one section in the box
            if (len(deck_thicknesses) > 1){
                for (i=[0:len(deck_thicknesses) - 2]){
                    translate([0, i_t_xy + sum(deck_thicknesses, 0, i) + i * deck_spacer, i_t_z])
                        cube([l, deck_spacer, h], true);
                }
            }  
        }
         //Add finger cutouts to the top of the box   
        translate([0, 0, h/2 + 9])rotate(a=[90,0,0])cylinder(width + 2*wall_bottom,grab_cutout_radius,grab_cutout_radius, true);
         //Add sideslots, if selected
        if (side_slots == "yes")
        {
         //side slots
            translate([l/2, s_t_y, s_t_z])
                cube([s_x, s_y, s_z]);     
            translate([-l/2-wall_bottom, s_t_y, s_t_z])cube([s_x, s_y, s_z]);
       
        }   
    }
}

//--------------------------------------------------------------------------------------------------------------------
module top(){
    //create outside box
    difference(){
        //create outside box
        minkowski(){
            cube([o_x, o_y, top_depth], true);
            sphere(fillet);    
        }   
        //create inside hole of top
        translate([0, 0, wall_bottom])cube([it_x, it_y, top_depth + 2*fillet], true);       
    }
}

//--------------------------------------------------------------------------------------------------------------------
module honeycomb(max_width, max_length, height, cell_size, wall_thickness, min_border) {
    cell_width = (sqrt(3)/2)*(cell_size + wall_thickness);  
    //now subroutines actually put the center of the first hexagon at the orgin so we need to take those amounts into account
    //when calculating the total size of the hexagons grid created
    x_comb_inner_shift = (0.5*(cell_size*2)/sqrt(3));  //amount to get hexagons to edge of square, so add this amount
    y_comb_inner_shift = (0.5*cell_size); //amount to get hexagons to edge of square, so add this amount 
    
    //max width - border and origin shift of hexagon grid
    max_comb_width = max_width - (2*min_border) - x_comb_inner_shift + 2*wall_thickness;
    columns = floor(max_comb_width / cell_width);
    
    //max column length has to take into account the extra half cell height of alternate rows and origin shift of hexagon grid
    max_comb_length = max_length - (2*min_border) - (0.5*(cell_size + wall_thickness)) - y_comb_inner_shift;
    rows = floor(max_comb_length / (cell_size + wall_thickness));  
    
    //x translation to put hexagons in middle of square
    x_actual_comb_size = columns*(cell_width);    
    translation_x = ((max_width - x_actual_comb_size) / 2) + x_comb_inner_shift;
    
    //y translation to put hexagons in middle of square
    y_actual_comb_size = rows*(cell_size + wall_thickness) + 0.5*(cell_size + wall_thickness); //rows + plus the extra half cell height       
    translation_y = ((max_length - y_actual_comb_size) / 2) + y_comb_inner_shift + wall_thickness/2;
    
    translate([translation_x,translation_y,0])hc_rows(columns, rows, height, cell_size, wall_thickness);  
}
//--------------------------------------------------------------------------------------------------------------------
module hc_rows (no_of_columns, no_of_rows, height, cell_size, wall_thickness) {   
    //no_of_rows = floor(1.2 * length / (cell_size + wall_thickness)) ;
    tr_mod = cell_size + wall_thickness;
    tr_x = sqrt(3)/2 * tr_mod;
    tr_y = tr_mod / 2;
    off_x = -1 * wall_thickness / 2;
    //off_y = wall_thickness / 2;
    linear_extrude(height = height, center = false, convexity = 10, twist = 0, slices = 1)
    for (i = [0 : (no_of_columns - 1)]) {
        translate([i * tr_x, (i % 2) * tr_y,0])
        hc_columns(no_of_rows, cell_size, wall_thickness);
    }
}
//--------------------------------------------------------------------------------------------------------------------
module hc_columns(no_of_cells, cell_size, wall_thickness) {
    //no_of_cells = floor(col_length / (cell_size + wall_thickness)) ;
    for (i = [0 : (no_of_cells - 1)]) {
            translate([0,(i * (cell_size + wall_thickness)),0])
                     circle($fn = 6, r = cell_size * (sqrt(3)/3));
    }
}
//--------------------------------------------------------------------------------------------------------------------
module logo(logo_data,h, logo_scale) {
	if (isArray(logo_data)) {
		s = min(
            (l + wall_bottom) / logo_data[0][0], 
            (o_h + wall_bottom) / logo_data[0][1]) 
            * logo_scale / 100;
//
//foo1 =  (l + wall_bottom) / logo_data[0][0];
//foo2 =  (o_h + wall_bottom) / logo_data[0][1];     
//echo(foo1=foo1);
//echo(foo2=foo2) ;
//echo (s=s);
		minX = min([for(i=[1:len(logo_data) - 1]) min([for(j=[0:len(logo_data[i][1]) - 1]) logo_data[i][1][j][0]])]);
		minY = min([for(i=[1:len(logo_data) - 1]) min([for(j=[0:len(logo_data[i][1]) - 1]) logo_data[i][1][j][1]])]);

        translate([-(logo_data[0][0] * s)/2, h/2, (logo_data[0][1] * s)/2])rotate([90,0,0])scale([s, -s, 1])
            difference() {
                union(){
                    for (i = [1:len(logo_data) - 1] ){
                        if (logo_data[i][0]==1){
                            linear_extrude(height = h) polygon(logo_data[i][1]);
                        }
                    }
                    for (i = [1:len(logo_data) - 1] ){
                        if (logo_data[i][0]==0){
                            linear_extrude(height = h * 2) polygon(logo_data[i][1]);
                        }
                    }
                }
            }
	}
}
