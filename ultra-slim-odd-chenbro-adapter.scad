// File
e = 0.001;
$fn = 15;

// Tray panel
tray_panel_depth = 3;
tray_panel_height = 10.5;

// Width
unit_width_full = 128;
unit_width_after_taper = 103;
unit_width_taper = unit_width_full - unit_width_after_taper;

// Depth
unit_depth_full = 126;
unit_depth_taper_from_front = 90;
unit_depth_taper_from_back = 11;
unit_depth_taper = unit_depth_full - unit_depth_taper_from_front;
unit_depth_with_tray = unit_depth_full + tray_panel_depth;

// Height
unit_height_full = 9.5;
unit_height_overhang = 5;
unit_height_under_overhang = unit_height_full - unit_height_overhang;

slot_height = 14.5;

adapter_height = slot_height - unit_height_full;

// SATA Ports
unit_sata_port_from_right = 37;
unit_sata_port_width = 31.5;

// Screw holes
unit_screw_hole_diameter = 1.5;

// Case
case_lip_depth = 9;
case_groove_bump_width = 9;
case_groove_bump_drop = 3.5;

case_screw_left = 33;
case_screw_right = 95;

module unit_screw_hole() {
  translate([0, 0, -90]){
    cylinder(r = (unit_screw_hole_diameter / 2) + 0.1, h = 92);
  }
}

module unit_screw_hole_back(from_left, from_bottom) {
  translate([unit_width_taper+from_left, 0, from_bottom]) {
    rotate ([-90, 0, 0]) {
      unit_screw_hole();
    }
  }
}

module unit_screw_hole_inner_left(from_back, from_bottom) {
  translate([unit_width_taper, from_back, from_bottom]) {
    rotate ([0, 90, 0]) {
      unit_screw_hole();
    }
  }
}

module unit_screw_hole_top(from_back, from_right) {
  translate([unit_width_full - from_right, from_back, unit_height_full]) {
    rotate ([180, 0, 0]) {
      unit_screw_hole();
    }
  }
}

module case_screw_hole_top(from_back, from_right){
  translate([unit_width_full - from_right, from_back, unit_height_full + 1]){
    cylinder(r=1.75, h=50);
  }
}

module unit_screw_holes(){
  unit_screw_hole_back(5, 3);
  unit_screw_hole_back(37, 3);
  unit_screw_hole_back(49.5, 3);
  unit_screw_hole_back(98.5, 5.5);
  unit_screw_hole_inner_left(15.5, 1.5);
  unit_screw_hole_inner_left(98.5, 1.5);
  unit_screw_hole_inner_left(120.5, 1.5);
  unit_screw_hole_top(4, 100);
  unit_screw_hole_top(120, 2);
}

module case_screw_holes(){
  case_screw_hole_top(12, case_screw_left);
  case_screw_hole_top(89, case_screw_left);

  case_screw_hole_top(12, case_screw_right);
  case_screw_hole_top(89, case_screw_right);
}

module unit(){
  difference() {
    union(){
      cube([unit_width_full, unit_depth_full, unit_height_full]);
      translate([0, unit_depth_full, 0]){
        cube([unit_width_full, tray_panel_depth, tray_panel_height]);
      }
    }

    translate([-e, -e, -e]) scale ([1+e, 1+e, 1+e]) {
      cube([unit_width_taper, unit_depth_taper_from_back, unit_height_full]);
      linear_extrude(height = unit_height_full) {
        polygon(points = [[0, unit_depth_taper_from_back],
                          [unit_width_taper, unit_depth_taper_from_back],
                          [0, unit_depth_taper]]);

      }
      cube([unit_width_taper, unit_depth_full, unit_height_under_overhang]);
    }
  }
}

module adapter_tray_hood (gap){
  translate([0, unit_depth_full, tray_panel_height + gap]){
    cube([unit_width_full, tray_panel_depth, adapter_height - (tray_panel_height - unit_height_full + gap)]);
  }
}

module adapter_groove(back_width){
  translate([unit_width_full - case_groove_bump_width, -1 * back_width,
             adapter_height + unit_height_full - case_groove_bump_drop]){
    cube([case_groove_bump_width, unit_depth_with_tray - 15 + back_width, case_groove_bump_drop]);
  }
}

module adapter_top_cutout(from_hole, from_back){
  translate([unit_width_full - case_screw_right + from_hole, from_back, unit_height_full - 1]){
    cube([case_screw_right - case_screw_left - (from_hole * 2),
          unit_depth_full - (from_back * 2),
          unit_height_full + 2]);
  }
}

module adapter(back_width){
  color([0.1,0.1,0.1]){
    difference() {
      union() {
        translate([unit_width_taper, 0, unit_height_full]) {
          cube([unit_width_after_taper, unit_depth_full, adapter_height]);
        }
        translate([unit_width_taper,-1*back_width,0]){
          cube([unit_width_after_taper, back_width, unit_height_full + adapter_height]);
        }
        translate([0, unit_depth_full - case_lip_depth, unit_height_full ]){
          cube([unit_width_taper, case_lip_depth, adapter_height]);
        }
        adapter_tray_hood(0.2);
      }
      adapter_groove(back_width);
      adapter_top_cutout(8,10);
      translate([unit_width_full - unit_sata_port_from_right, 5, 0]) {
        rotate ([-270, 0, 0]) {
          cube([unit_sata_port_width, 10, unit_height_full]);
        }
      }
    }
  }
}

difference() {
  union() {
    unit();
    adapter(2);
  }
  unit_screw_holes();
  case_screw_holes();
}
