//include <../../../lib/MCAD/regular_shapes.scad>

// Lot Parameters
property_grade = 12;
frontage_ft = 91;


// Math
function get(m, k) =
  let (idx = search([k], m))
  m[idx[0]][1];

function computed_params(m) =
  let(length       = get(m, "length"),
      width        = get(m, "width"),
      anchor_to    = get(m, "anchor_to"),
      anchored     = is_list(anchor_to),
      center_x     = get(m, "center_x"),
      center_y     = get(m, "center_y"),
      center_z     = tan(property_grade) * center_y,
      center_depth = tan(property_grade) * (length / 2),
      pos_x        = center_x - width/2 ,
      pos_y        = center_y - length/2 ,
      pos_z        = anchored ? get(anchor_to, "pos_z") : center_z - center_depth)
  concat(m, [["center_z",     tan(property_grade) * center_y],
             ["center_depth", center_depth],
             ["pos_x",        pos_x],
             ["pos_y",        pos_y],
             ["pos_z",        pos_z],
             ["pos",          [pos_x, pos_y, pos_z]]]);

// House Parameters
house_NS =
  computed_params([["width",          25],
                   ["length",         40],
                   ["center_y",       125],
                   ["center_x",       45],
                   ["ceiling_height", 9],
                   ["stories",        3]]);

house_EW_length = 25;

house_EW =
  computed_params([["width",          get(house_NS, "width") * 1.5],
                   ["length",         house_EW_length],
                   ["anchor_to",      house_NS],
                   ["center_y",       get(house_NS, "center_y") + get(house_NS, "length") / 2 - house_EW_length / 2],
                   ["center_x",       get(house_NS, "center_x")],
                   ["ceiling_height", get(house_NS, "ceiling_height")],
                   ["stories",        get(house_NS, "stories")]]);

garage =
  computed_params([["width",          25],
                   ["length",         25],
                   ["center_y",       40],
                   ["center_x",       78],
                   ["ceiling_height", 9],
                   ["stories",        1]]) ;


// Rendering helpers
module cutout() {
  let (e = 1.02) {
    scale([e,e,e]) translate([-e,-e,0]) children();
  }
}

// Excavation
module excavation(m){
  let(ceiling_height = get(m, "ceiling_height"),
      stories        = get(m, "stories"),
      pos            = get(m, "pos"),
      width          = get(m, "width"),
      length         = get(m, "length"),
      height         = stories * ceiling_height) {
    translate(pos){
      cube([width,length,height]);
    }
  }
}

// Lot
module property(){
  let (pixel_length    = 161,
       pixel_width     = 82,
       frontage_pixels = 72,
       feet_per_pixel  = frontage_ft / frontage_pixels,
       back_elevation  = tan(property_grade) * pixel_length) {
    scale([feet_per_pixel,feet_per_pixel,feet_per_pixel]) color("ForestGreen") difference(){
      linear_extrude(height=back_elevation){
        translate([0, pixel_length, back_elevation]) mirror([0,1,0]){
          polygon([[0, pixel_length], [72,159], [pixel_width,9], [35,2], [16,38], [7,151], [0,153]]);
        }
      }
      cutout() rotate([property_grade,0,0]){
        cube([pixel_width, pixel_length*1.02, back_elevation]);
      }
    }
  }
}

module property_excavated(){
  difference(){
    property();
    excavation(house_NS);
    excavation(house_EW);
    excavation(garage);
  }
  excavation(house_NS);
  excavation(house_EW);
  excavation(garage);
}

property_excavated();
